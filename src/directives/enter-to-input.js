// 场景： enter-to-input 自定义指令可以应用于需要在输入框中按下回车键时触发特定操作的场景，例如：

// 搜索框回车搜索：当用户在搜索框中输入关键字后，按下回车键时，可以触发搜索操作，快速获取搜索结果。
// 发送消息：当用户在聊天应用中输入完消息后，按下回车键时，可以触发发送消息操作，方便快捷地发送消息。
// 提交表单：当用户在表单中填写完信息后，按下回车键时，可以触发提交表单操作，快速提交表单信息。

// 总之，enter-to-input 自定义指令可以帮助我们实现在输入框中按下回车键时触发特定操作的功能，提升用户体验和操作效率。
const enterToInput = {
  inserted: function (el) {
    let inputs = el.querySelectorAll('input')
    // 绑定回写事件
    for (var i = 0; i < inputs.length; i++) {
      inputs[i].setAttribute('keyFocusIndex', i)
      inputs[i].addEventListener('keyup', (ev) => {
        if (ev.keyCode === 13) {
          const targetTo = ev.srcElement.getAttribute('keyFocusTo')
          if (targetTo) {
            this.$refs[targetTo].$el.focus()
          } else {
            var attrIndex = ev.srcElement.getAttribute('keyFocusIndex')
            var ctlI = parseInt(attrIndex)
            inputs = el.querySelectorAll('input')
            if (ctlI < inputs.length - 1) inputs[ctlI + 1].focus()
          }
        }
      })
    }
  }
}

export default enterToInput
