import html2Canvas from 'html2canvas'
import JsPDF from 'jspdf'
// https://juejin.cn/post/7012049739482923039#heading-1
// 通过html2canvas将HTML页面转换成canvas图片,再通过jspdf将图片转成pdf格式文件。
// 缺点：
//1. 清晰度不够
//2. 分页时,内容会被截断,不能智能展示完整
//3. 页面内容太长时（宽高限制为 14400），无法打印出内容，一直是空白页。这个真的就很不友好了呀，报表的内容肯定多呀...

// 注意点：
// 引入外链图片时，需要配置图片跨域，给 img 标签设置 crossOrigin='anonymous'。
// 提高生成图片质量，可以适当放大 canvas 画布，通过设置 scale 缩放画布大小，或者设置 dpi 提高清晰度。

// 导出页面为PDF格式
// id-导出pdf的div容器；title-导出文件标题
const htmlToPdf = (id, title) => {
  const element = document.getElementById(`${id}`)
  const opts = {
    scale: 12, // 缩放比例，提高生成图片清晰度
    useCORS: true, // 允许加载跨域的图片
    allowTaint: false, // 允许图片跨域，和 useCORS 二者不可共同使用
    tainttest: true, // 检测每张图片已经加载完成
    logging: true // 日志开关，发布的时候记得改成 false
  }
  html2Canvas(element, opts)
    .then((canvas) => {
      const contentWidth = canvas.width
      const contentHeight = canvas.height
      // 一页pdf显示html页面生成的canvas高度;
      const pageHeight = (contentWidth / 592.28) * 841.89
      console.log(pageHeight)
      // 未生成pdf的html页面高度
      let leftHeight = contentHeight
      // 页面偏移
      let position = 0
      // a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
      const imgWidth = 595.28
      const imgHeight = (592.28 / contentWidth) * contentHeight
      const pageData = canvas.toDataURL('image/jpeg', 1.0)
      // a4纸纵向，一般默认使用；new JsPDF('landscape'); 横向页面
      const PDF = new JsPDF('', 'pt', 'a4')
      // 当内容未超过pdf一页显示的范围，无需分页
      if (leftHeight < pageHeight) {
        // addImage(pageData, 'JPEG', 左，上，宽度，高度)设置
        PDF.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight)
      } else {
        // 超过一页时，分页打印（每页高度841.89）
        while (leftHeight > 0) {
          PDF.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight)
          leftHeight -= pageHeight
          position -= 841.89
          if (leftHeight > 0) {
            PDF.addPage()
          }
        }
      }
      PDF.save(title + '.pdf')
    })
    .catch((error) => {
      console.log('打印失败', error)
    })
}
export default htmlToPdf
