const plugins = []
plugins.push([
  'import',
  {
    libraryName: 'ant-design-vue',
    libraryDirectory: 'es',
    style: true // `style: true` 会加载less文件
  }
])
// 明确指定
module.exports = {
  //  vue cli项目babel配置解析
  //  https://juejin.cn/post/6906362999703863304
  //  presets 为预设数组
  presets: [
    '@vue/cli-plugin-babel/preset',
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'entry',
        corejs: 3 // 指定corejs 的版本,如果package.json没有core-js，还需要另外安装
      }
    ]
  ],
  // plugins为插件数组
  plugins
}
